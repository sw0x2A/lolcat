# lolcat

Simplified implementation of [lolcat](https://github.com/busyloop/lolcat) in [~~Go~~](https://gitlab.com/sw0x2A/lolcat/-/tree/golang) Rust. 

![](http://i3.photobucket.com/albums/y83/SpaceGirl3900/LOLCat-Rainbow.jpg)
