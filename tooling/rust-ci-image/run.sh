#!/bin/bash
VERSION="1.66.1"
IMAGE_URL="registry.gitlab.com/sw0x2a/lolcat/rust-ci-image"

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

configure() {
  cat <<EOF >"${SCRIPT_DIR}/Dockerfile"
FROM rust:${VERSION}

RUN apt-get update && apt-get install -y build-essential gcc-x86-64-linux-gnu curl

RUN cargo install cargo-audit
RUN cargo install cargo-deb

RUN rustup component add rustfmt
RUN rustup component add clippy
RUN rustup target add aarch64-unknown-linux-gnu
RUN rustup target add x86_64-unknown-linux-gnu
RUN rustup target add aarch64-apple-darwin
RUN rustup target add x86_64-apple-darwin
EOF
}

make() {
  docker build -t "${IMAGE_URL}:${VERSION}" -t "${IMAGE_URL}:latest" "${SCRIPT_DIR}"
  docker push "${IMAGE_URL}:${VERSION}"
  docker push "${IMAGE_URL}:latest"
}

clean() {
  rm "${SCRIPT_DIR}/Dockerfile"
}

# Main
configure
make
clean
